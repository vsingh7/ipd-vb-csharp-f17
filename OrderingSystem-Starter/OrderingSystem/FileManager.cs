﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace OrderingSystem
{
    public class FileManager
    {
        public string CustomerFileName { get; set; }
        public string ProductFileName { get; set; }

        OpenFileDialog openFileDialog = new OpenFileDialog();
        SaveFileDialog saveFileDialog = new SaveFileDialog();

        public List<Customer> loadCustomerFile()
        {
            List<Customer> customers = new List<Customer>();

            /* TODO:
             * - Load the data from a file chosen by the openFileDialog
             * - Assign the data to our customers list.
             * - Return the customers list.
             */

            return customers;
        }

        public void saveCustomerFile(List<Customer> customers)
        {
            /* TODO:
             * - Save the current customers list that has been passed in.
             */
        }

        public List<Product> loadProductFile()
        {
            List<Product> products = new List<Product>();

            /* TODO:
             * - Load the data from a file chosen by the openFileDialog
             * - Assign the data to our products list.
             * - Return the products list.
             */

            return products;
        }

        public void saveProductFile(List<Product> products)
        {
            /* TODO:
             * - Save the current products list that has been passed in.
             */
        }
    }
}
