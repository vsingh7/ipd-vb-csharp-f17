﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace OrderingSystem
{
    public partial class ProductDataWindow : Window
    {
        FileManager fileManager;
        public List<Product> products { get; set; }

        public ProductDataWindow(/* This is where we get our handles to the fileManager and products list */)
        {
            InitializeComponent();

            /* TODO:
             * - Initialize our fileManager and products list property.
             * - Bind to the product data grid control.
             */
        }

        private void btnLoad_Click(object sender, RoutedEventArgs e)
        {
            /* TODO:
             * - Use the appropriate load method from our fileManager to load the products.
             * - Bind to the product data grid control.
             */
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            /* TODO:
             * - Use the appropriate save method from our fileManager to save the products.
             */
        }
    }
}
