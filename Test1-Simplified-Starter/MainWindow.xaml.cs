﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Test1
{
    public partial class MainWindow : Window
    {
        ShoppingCart shoppingCart = new ShoppingCart();

        public MainWindow()
        {
            InitializeComponent();

            // TODO: When the app starts for the first time, we display "$0.00" for total cost;
        }

        private void btnApple_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void btnBanana_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void btnOrange_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void btnReset_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void rbtnCash_Checked(object sender, RoutedEventArgs e)
        {
            
        }

        private void rbtnCash_Unchecked(object sender, RoutedEventArgs e)
        {
            
        }

        private void btnCheckout_Click(object sender, RoutedEventArgs e)
        {
            checkout();
        }

        private void checkout()
        {
            /* TODO:
             * - If cash was selected:
             *      - Is there anything in the cash textbox?
             *      - If so, is it enough cash?
             * - If credit/debit was selected, does the total meet the $5.00 requirement?
             * - Is the shopping cart empty? If not, generate a receipt using ShoppingCart's checkout() function.
             */
        }
    }
}
