﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test1
{
    class ShoppingCart
    {
        private const double COST_APPLE = 0.50;
        private const double COST_BANANA = 0.75;
        private const double COST_ORANGE = 1.00;
        
        private int apples;
        private int bananas;
        private int oranges;

        public double cost;

        public ShoppingCart()
        {
            apples = 0;
            bananas = 0;
            oranges = 0;
            cost = 0;
        }

        // TODO: Write a function that will update the quantity of the product (one function per product).

        public void reset()
        {
            // TODO: Reset counters and total to zero.
        }

        private void calculateTotal()
        {
            // TODO: Calculate the total price by using the product counters and price constants.
        }

        public string checkout(string paymentType, double cashGiven)
        {
            string receipt = "";

            /* TODO:
             * - Check the quantity of each item added to the cart.
             * - Calculate the total of each item added.             
             * - List the total cost and payment type. If payment type is cash, calculate and print the amount of change needed (printChange function below).
             * - Append the information to the receipt string using the += operator and \n for newline.
             */

            receipt += "Thanks for shopping at the John Abbott College Grocery Store!";

            return receipt;
        }

        public bool isEmpty()
        {
            // TODO: Return if shopping cart is empty by checking the quantity of all the items.

            return true;
        }

        private string printChange(double amountGiven)
        {
            string change = "";

            /* TODO:
             * - Construct a message with the amount of change we need to give back to the customer using the total cost and amount given.
             * - If the amount given is the same as the total, say something like "They gave exact change!".
             */

            return change;
        }
    }
}
