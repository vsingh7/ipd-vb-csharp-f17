﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MultiForm_File_IO_Demo
{
    /// <summary>
    /// Interaction logic for DataWindow.xaml
    /// </summary>
    public partial class DataWindow : Window
    {
        List<Person> people;

        public DataWindow(List<Person> pPeople)
        {
            InitializeComponent();

            people = pPeople;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            foreach (Person person in people)
            {
                lbxNames.Items.Add(person.FirstName + " " + person.LastName);
            }
        }

        private void lbxNames_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            MessageBox.Show("Selection was changed: " + e.ToString());
        }
    }
}
