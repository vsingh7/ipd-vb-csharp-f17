﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace OrderingSystem
{
    public class FileManager
    {
        public string CustomerFileName { get; set; }
        public string ProductFileName { get; set; }

        OpenFileDialog openFileDialog = new OpenFileDialog();
        SaveFileDialog saveFileDialog = new SaveFileDialog();

        public List<Customer> loadCustomerFile()
        {
            List<Customer> customers = new List<Customer>();

            openFileDialog.ShowDialog();

            try
            {
                CustomerFileName = openFileDialog.FileName;

                if (File.Exists(CustomerFileName) && CustomerFileName.EndsWith(".csv"))
                {
                    StreamReader inFile = new StreamReader(CustomerFileName);

                    while (!inFile.EndOfStream)
                    {
                        String aLine = inFile.ReadLine();
                        String[] theParts = aLine.Split(',');
                        Customer customer = new Customer();
                        customer.LastName = theParts[0];
                        customer.FirstName = theParts[1];
                        customers.Add(customer);
                    }

                    inFile.Close();
                    MessageBox.Show("File has been loaded.");
                }
                else
                {
                    MessageBox.Show("Invalid Filename.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
            return customers;
        }

        public void saveCustomerFile(List<Customer> customers)
        {
            try
            {
                StreamWriter outFile = new StreamWriter(CustomerFileName);

                foreach (Customer customer in customers)
                {
                    outFile.WriteLine($"{customer.FirstName},{customer.LastName},");
                }

                outFile.Close();
                MessageBox.Show("File has been saved.");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public List<Product> loadProductFile()
        {
            List<Product> products = new List<Product>();

            openFileDialog.ShowDialog();

            try
            {
                ProductFileName = openFileDialog.FileName;

                if (File.Exists(ProductFileName) && ProductFileName.EndsWith(".csv"))
                {
                    StreamReader inFile = new StreamReader(ProductFileName);

                    while (!inFile.EndOfStream)
                    {
                        String aLine = inFile.ReadLine();
                        String[] theParts = aLine.Split(',');
                        Product product = new Product();
                        product.Name = theParts[0];
                        product.Price = Double.Parse(theParts[1]);
                        products.Add(product);
                    }

                    inFile.Close();
                    MessageBox.Show("File has been loaded.");
                }
                else
                {
                    MessageBox.Show("Invalid Filename.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return products;
        }

        public void saveProductFile(List<Product> products)
        {
            try
            {
                StreamWriter outFile = new StreamWriter(ProductFileName);

                foreach (Product product in products)
                {
                    outFile.WriteLine($"{product.Name},{product.Price.ToString()},");
                }

                outFile.Close();
                MessageBox.Show("File has been saved.");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
