﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace OrderingSystem
{
    public partial class CustomerDataWindow : Window
    {
        FileManager fileManager = new FileManager();
        public List<Customer> customers { get; set; }

        public CustomerDataWindow(List<Customer> existingCustomers)
        {
            InitializeComponent();

            this.customers = existingCustomers;
            dataGridCustomers.ItemsSource = customers;
        }

        private void btnLoad_Click(object sender, RoutedEventArgs e)
        {
            customers = fileManager.loadCustomerFile();
            dataGridCustomers.ItemsSource = customers;
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            fileManager.saveCustomerFile(customers);
        }

        private void btnSaveAs_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
