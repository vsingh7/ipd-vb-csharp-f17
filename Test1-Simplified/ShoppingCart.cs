﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test1
{
    class ShoppingCart
    {
        private const double COST_APPLE = 0.50;
        private const double COST_BANANA = 0.75;
        private const double COST_ORANGE = 1.00;
        
        private int apples;
        private int bananas;
        private int oranges;

        public double cost;

        public ShoppingCart()
        {
            apples = 0;
            bananas = 0;
            oranges = 0;
            cost = 0;
        }

        public void reset()
        {
            apples = 0;
            bananas = 0;
            oranges = 0;

            calculate();
        }

        private void calculate()
        {
            cost = apples * COST_APPLE +
                bananas * COST_BANANA +
                oranges * COST_ORANGE;
        }

        public void addApple()
        {
            apples++;
            calculate();
        }

        public void addBanana()
        {
            bananas++;
            calculate();
        }

        public void addOrange()
        {
            oranges++;
            calculate();
        }

        public string checkout(string paymentType, double cashGiven)
        {
            string receipt = "";

            if (apples > 0)
            {
                receipt += apples.ToString() + " apple(s) ... " + (apples * COST_APPLE).ToString("C") + "\n";
            }

            if (bananas > 0)
            {
                receipt += bananas.ToString() + " banana(s) ... " + (bananas * COST_BANANA).ToString("C") + "\n";
            }

            if (oranges > 0)
            {
                receipt += oranges.ToString() + " orange(s) ... " + (oranges * COST_ORANGE).ToString("C") + "\n";
            }

            receipt += "\n";
            receipt += "Payment Type: " + paymentType + "\n\n";
            receipt += "Total cost: " + cost.ToString("C") + "\n";
            
            if (paymentType.Equals("Cash"))
            {
                receipt += "Amount received: " + cashGiven.ToString("C") + "\n";
                receipt += printChange(cashGiven);
            }

            receipt += "\n";

            receipt += "Thanks for shopping at the John Abbott College Grocery Store!";

            return receipt;
        }

        public bool isEmpty()
        {
            return (apples == 0 && 
                bananas == 0 && 
                oranges == 0);
        }

        private string printChange(double amountGiven)
        {
            double change = amountGiven - cost;
            string changeBreakdown = "";

            if (change > 0)
            {
                changeBreakdown += "Change: "+ change.ToString("C") +"\n\n";
            }
            else
            {
                changeBreakdown += "They gave exact change!\n";
            }

            return changeBreakdown;
        }
    }
}
