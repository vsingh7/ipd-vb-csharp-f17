﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Test1
{
    public partial class MainWindow : Window
    {
        ShoppingCart shoppingCart = new ShoppingCart();

        public MainWindow()
        {
            InitializeComponent();

            txtTotal.Text = shoppingCart.cost.ToString("C");
        }

        private void btnApple_Click(object sender, RoutedEventArgs e)
        {
            shoppingCart.addApple();
            txtTotal.Text = shoppingCart.cost.ToString("C");
        }

        private void btnBanana_Click(object sender, RoutedEventArgs e)
        {
            shoppingCart.addBanana();
            txtTotal.Text = shoppingCart.cost.ToString("C");
        }

        private void btnOrange_Click(object sender, RoutedEventArgs e)
        {
            shoppingCart.addOrange();
            txtTotal.Text = shoppingCart.cost.ToString("C");
        }

        private void btnReset_Click(object sender, RoutedEventArgs e)
        {
            shoppingCart.reset();
            txtTotal.Text = shoppingCart.cost.ToString("C");
            rbtnCash.IsChecked = false;
            rbtnCreditDebit.IsChecked = false;
            txtCash.IsEnabled = false;
            txtCash.Text = "";
        }

        private void rbtnCash_Checked(object sender, RoutedEventArgs e)
        {
            txtCash.IsEnabled = true;
        }

        private void rbtnCash_Unchecked(object sender, RoutedEventArgs e)
        {
            txtCash.IsEnabled = false;
        }

        private void btnCheckout_Click(object sender, RoutedEventArgs e)
        {
            checkout();
        }

        private void checkout()
        {
            string paymentType;
            double cashAmount = 0.00;

            if (rbtnCash.IsChecked == true)
            {
                string cash = txtCash.Text;

                if (cash.Equals(""))
                {
                    MessageBox.Show("Please enter an amount for cash");
                    return;
                }
                else
                {
                    cashAmount = Double.Parse(txtCash.Text);

                    if (cashAmount < shoppingCart.cost)
                    {
                        MessageBox.Show("Not enough cash provided.");
                        return;
                    }
                    else
                    {
                        paymentType = "Cash";
                    }
                }
            }
            else if (rbtnCreditDebit.IsChecked == true)
            {
                if (shoppingCart.cost >= 5.00)
                {
                    paymentType = "Credit/Debit";
                }
                else
                {
                    MessageBox.Show("Total cost must be >= $5.00 to use credit or debit.");
                    return;
                }
            }
            else
            {
                MessageBox.Show("Please select a payment type.");
                return;
            }

            if (shoppingCart.isEmpty())
            {
                MessageBox.Show("Shopping cart is empty!", "John Abbott College Grocery Store Receipt");
            }
            else
            {
                MessageBox.Show(shoppingCart.checkout(paymentType, cashAmount), "John Abbott College Grocery Store Receipt");
            }
        }
    }
}
